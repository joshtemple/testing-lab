"""Generates and runs test cases from a directory containing SQL tests."""

from pathlib import Path
import unittest
import sqlite3

class SqlTest(unittest.TestCase):
    """Runs a SQL test from file against the inspections database.

    :param Path query_path: The file location of the SQL test query.

    """
    def __init__(self, query_path: Path) -> None:
        method_name = 'test_' + query_path.stem
        def test_method():
            with open(self.query_path, 'r') as file:
                query = file.read()
            result = self.connection.execute(query).fetchone()
            self.assertIsNone(result)

        setattr(self, method_name, test_method)
        super(SqlTest, self).__init__(method_name)
        self.query_path = query_path

    def setUp(self) -> None:
        self.connection = sqlite3.connect('data/inspections.db')

    def tearDown(self) -> None:
        self.connection.close()

def populate_test_suite(test_dir: str) -> unittest.TestSuite:
    """Build a test suite with SqlTests generated from test queries from file.

    :param str test_dir: Path to the folder containing SQL test queries.
    :return: Test suite populated with corresponding SQL test cases.
    :rtype: unittest.TestSuite

    """
    query_paths = Path(test_dir).glob('*.sql')
    suite = unittest.TestSuite()
    suite.addTests(SqlTest(query_path) for query_path in query_paths)
    return suite
