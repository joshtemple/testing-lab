"""Implements a custom load_test method to discover data tests as well."""

from pathlib import Path
from test.test_data import populate_test_suite

def load_tests(loader, standard_tests, pattern):
    """Used by python -m unittest discover to load all test suites."""
    unittest_dir = Path('test')
    sql_test_dir = Path('sql_tests')
    package_tests = loader.discover(start_dir=unittest_dir, pattern=pattern)
    standard_tests.addTests(package_tests)
    data_test_suite = populate_test_suite(sql_test_dir)
    standard_tests.addTests(data_test_suite)
    return standard_tests
