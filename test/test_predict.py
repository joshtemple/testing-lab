import unittest
import datetime
import pandas as pd
from pandas.testing import assert_series_equal
import predict

class PredictTest(unittest.TestCase):
    def test_row_number(self):
        df = pd.DataFrame([
            {'id': 'A', 'day': 10},
            {'id': 'B', 'day': 3},
            {'id': 'A', 'day': 9},
            {'id': 'A', 'day': 4},
            {'id': 'B', 'day': 13}])

        to_test = predict.row_number(
            df,
            partition_by=['id'],
            order_by=['id', 'day'],
            ascending=[True, True])

        to_test = to_test.reset_index(drop=True)
        correct = pd.Series([1, 2, 3, 1, 2])
        assert_series_equal(to_test, correct)

    def test_predict_score(self):
        df = pd.DataFrame([
            {'id': 'A', 'inspection_date': datetime.date(2015, 1, 10), 'score': 7, 'sequence': 1},
            {'id': 'B', 'inspection_date': datetime.date(2017, 10, 5), 'score': 32, 'sequence': 1},
            {'id': 'A', 'inspection_date': datetime.date(2016, 3, 22), 'score': 12, 'sequence': 2},
            {'id': 'A', 'inspection_date': datetime.date(2017, 11, 12), 'score': 8, 'sequence': 3},
            {'id': 'B', 'inspection_date': datetime.date(2018, 12, 1), 'score': 18, 'sequence': 2},
            {'id': 'A', 'inspection_date': datetime.date(2018, 10, 5), 'score': 10, 'sequence': 4}])

        history = df[df['sequence'] > 1]
        to_test = history.groupby('id').apply(predict.predict_score)
        correct = pd.Series([9.57243244, 18], index=['A', 'B'])
        correct.index.name = 'id'
        assert_series_equal(to_test, correct)

    def test_mae_is_always_positive(self):
        df = pd.DataFrame([
            {'score': 1, 'predicted': 10},
            {'score': 1, 'predicted': -10},
            {'score': 1, 'predicted': 1}])

        mae = predict.calculate_mae(df)
        self.assertGreaterEqual(mae, 0)
        self.assertEqual(mae, sum([9, 11, 0]) / 3)

if __name__ == '__main__':
    unittest.main()
