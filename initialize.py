"""Set up a SQLite database on disk with the DOHMH inspections dataset."""

from pathlib import Path
import csv
import sqlite3
import urllib.request
from logger import logger

def download_data(download_path: Path) -> None:
    """Download the DOHMH inspections dataset to a specified file location.

    :param Path download_path: File location to download the inspections CSV to.

    """
    url = 'https://data.cityofnewyork.us/api/views/43nn-pn8j/rows.csv?accessType=DOWNLOAD'
    logger.info(f'Downloading dataset from {url}')
    urllib.request.urlretrieve(url, download_path)

def execute_from_file(connection: sqlite3.Connection, query_path: Path) -> None:
    """Execute a SQL query from file. The query must end with a semicolon.

    :param sqlite3.Connection connection: Connection to the database.
    :param Path query_path: File location of the SQL query to run.

    """
    with query_path.open('r') as file:
        query = file.read()
    connection.executescript(query)

def copy(connection: sqlite3.Connection, tablename: str, load_path: Path,
         skip_header_rows: int = 0) -> None:
    """Copy data from a CSV into a SQLite database table.

    :param sqlite3.Connection connection: Connection to the database.
    :param str tablename: The table in the database to be copied to.
    :param Path load_path: File location of the CSV to be loaded.
    :param int skip_header_rows: Number of header rows in the CSV file to skip.

    """
    with load_path.open('r') as file:
        reader = csv.reader(file)
        to_write = [tuple(row) for row in reader]
    to_write = to_write[skip_header_rows:]

    param_string = ', '.join(['?'] * len(to_write[0]))
    sql = f"INSERT INTO {tablename} VALUES ({param_string});"
    connection.executemany(sql, to_write)

if __name__ == '__main__':
    Path.mkdir(Path('data'), exist_ok=True)
    database_path = Path('data/inspections.db')
    create_table_path = Path('inspections.sql')
    csv_path = Path('data/inspections.csv')

    download_data(csv_path)
    logger.info(f'Connecting to SQLite database at {database_path}')
    with sqlite3.connect(database_path) as connection:
        logger.info('Creating the inspections table...')
        execute_from_file(connection, query_path=create_table_path)
        logger.info('...and populating it from CSV')
        copy(connection, tablename='inspections', load_path=csv_path, skip_header_rows=1)

    logger.info('Cleaning up extra files')
    csv_path.unlink()
    logger.info(f'Database successfully initialized at {database_path}')
