"""Implement module-level logger and formatting."""

import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

handler = logging.StreamHandler()
formatter = logging.Formatter(
    fmt='[%(asctime)s] %(levelname)s: %(message)s',
    datefmt='%H:%M:%S')
handler.setFormatter(formatter)
logger.addHandler(handler)
