"""Attempts to predict a restaurant's most recent score given its history."""

from pathlib import Path
import sqlite3
import pandas as pd
from logger import logger

def get_data_from_db() -> pd.DataFrame:
    """Extract average scores for each inspection and restaurant (by CAMIS).

    :return: Raw inspection scores extracted from the database.
    :rtype: pd.DataFrame

    """
    sql = """
      select
        camis,
        name,
        inspection_date,
        avg(score) as score
      from inspections
      group by 1,2,3
    """
    database_path = Path('data/inspections.db')
    connection = sqlite3.connect(database_path)
    df = pd.read_sql(sql, connection)
    connection.close()
    return df

def row_number(df: pd.DataFrame, partition_by: list,
               order_by: list, ascending: list) -> pd.DataFrame:
    """Generate an integer sequence for each partition by `order_by` columns.

    :param pd.DataFrame df: Input data to be sequenced.
    :param list partition_by: Groups to sequence independently.
    :param list order_by: Order in which to sequence each group.
    :param list ascending: List of booleans indicating ascending or descending order.
    :return: Original DataFrame with sequence applied.
    :rtype: pd.DataFrame

    """
    return df \
        .sort_values(by=order_by, ascending=ascending) \
        .groupby(partition_by) \
        .cumcount() + 1

def predict_score(df: pd.DataFrame, half_life=365) -> pd.DataFrame:
    """Predict the next inspection score based on a weighted average of the past.

    :param pd.DataFrame df: Description of parameter `df`.
    :param type half_life: Description of parameter `half_life`.
    :return: Description of returned object.
    :rtype: pd.DataFrame

    """
    last_inspection_date = df['inspection_date'].max()
    df['days_before_last_inspection'] = (last_inspection_date - df['inspection_date']).dt.days
    df['weight'] = 2 ** (-df['days_before_last_inspection'] / half_life)
    df['contribution'] = df['weight'] * df['score']
    predicted_score = df['contribution'].sum() / df['weight'].sum()
    return predicted_score

def calculate_mae(df: pd.DataFrame) -> float:
    """Calculate the mean average error of predictions.

    :param pd.DataFrame df: Input data with columns `score` and `predicted`.
    :return: Mean absolute error.
    :rtype: float

    """
    absolute_error = abs(df['score'] - df['predicted'])
    return absolute_error.mean()

if __name__ == '__main__':
    SAMPLE_SIZE = 10000
    logger.info('Fetching dataset from database.')
    df = get_data_from_db().head(SAMPLE_SIZE)

    logger.info('Parsing inspection date column to datetime.')
    df['inspection_date'] = pd.to_datetime(df['inspection_date'])

    logger.info('Sequencing each inspection and splitting out the most recent inspections.')
    df['inspection_sequence'] = row_number(
        df,
        partition_by=['camis'],
        order_by=['camis', 'inspection_date'],
        ascending=[True, False])
    history = df[df['inspection_sequence'] > 1]
    most_recent = df[df['inspection_sequence'] == 1]

    logger.info(
        'Calculating predicted scores of the first %s rows based on a weighted, ' \
        'time-decay average.',
        SAMPLE_SIZE)
    predicted = history.groupby('camis').apply(predict_score)
    predicted.name = 'predicted'
    results = most_recent.set_index('camis')[['name', 'score']].join(predicted)

    logger.info('Calculating mean absolute error of predictions.')
    mae = calculate_mae(results)
    logger.info('The mean absolute error of this model is %.1f', mae)
