drop table if exists `inspections`;
create table `inspections` (
  camis integer,
  name text,
  borough text,
  address_1 text,
  address_2 text,
  zip text,
  phone text,
  cuisine text,
  inspection_date int,
  action text,
  violation_code,
  violation_description float,
  critical_flag float,
  score float,
  grade text,
  grade_date text,
  record_date text,
  inspection_type text
);
