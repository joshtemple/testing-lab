
# Data Warehouse Testing Lab
This project is an interactive lab to practice testing approaches in the modern data warehouse. We'll be using a simple on-disk SQLite database as our data warehouse to test and query.

We'll be filling our "data warehouse" with a [dataset](https://data.cityofnewyork.us/Health/DOHMH-New-York-City-Restaurant-Inspection-Results/43nn-pn8j) from the New York City Department of Health & Mental Hygiene (DOHMH). This dataset details the results of restaurant health inspections in New York City.

### The dataset
The Department of Health supplies some interesting caveats and additional information about this dataset:

>The dataset contains every sustained or not yet adjudicated violation citation from every full or special program inspection conducted up to three years prior to the most recent inspection for restaurants and college cafeterias in an active status on the RECORD DATE (date of the data pull).
>
>When an inspection results in more than one violation, values for associated fields are repeated for each additional violation record.

We don't know if this is a primary key yet, but we know that each restaurant has a unique ID called CAMIS.
>Establishments are uniquely identified by their CAMIS (record ID) number.

>Keep in mind that thousands of restaurants start business and go out of business every year; only restaurants in an active status are included in the dataset.

>Records are also included for each restaurant that has applied for a permit but has not yet been inspected and for inspections resulting in no violations. Establishments with inspection date of 1/1/1900 are new establishments that have not yet received an inspection.

>Restaurants that received no violations are represented by a single row and coded as having no violations using the ACTION field.

We're especially interested in this line from the dataset's description:
>Because this dataset is compiled from several large administrative data systems, it contains some illogical values that could be a result of data entry or transfer errors. Data may also be missing.

This means we'll have lots of assumptions to tests and potential inconsistencies to find and account for. Let's dive in!

### Set up your environment
Clone this repository using the following command:
```
git clone https://gitlab.com/joshtemple/testing-lab.git
```
You'll need to use a Python 3.7+ environment with `pandas` installed to run the prediction code in this project.

You can do this however you prefer, but I like to keep project dependencies separate from my system Python install, so I use [`pyenv`](https://github.com/pyenv/pyenv) and [`pyenv virtualenv`](https://github.com/pyenv/pyenv-virtualenv) to manage my virtual environments. The general approach is to create a virtual environment for each project and then install dependencies for that project only to keep things clean and organized.

Once your virtual environment is created and activated, install dependencies by running the following command in the cloned repository directory on your machine.
```
pip install -r requirements.txt
```
This will install all packages listed in the `requirements.txt` file. In this case, `pip` will only install `pandas`, but sometimes projects can have a lot of package dependencies, and this file is a great way to organize them.

### Initialize the SQLite database
Initialize the database by running the `initialize.py` script as follows:
```
python3 initialize.py
```
You should see that the file is downloaded from [data.cityofnewyork.us](https://opendata.cityofnewyork.us/) and is loaded into a SQLite database at `data/inspections.db`. You can connect to the database using the SQLite command-line interface. See here for [installation instructions](http://www.sqlitetutorial.net/download-install-sqlite/) and [downloads](https://www.sqlite.org/download.html) if you don't have SQLite installed.

Connect to the SQLite database from your command line and test that the table has been loaded correctly:
```
sqlite3 data/inspections.db
sqlite> select count(*) from inspections;
```
SQLite has a few helpful [dot-prefixed commands](https://sqlite.org/cli.html) that you can review. `.tables` is especially useful to see what tables exist in your database. Use `.exit` to exit the SQLite interface and return to your command line.

### Testing the data
This project comes with a simple test framework for running SQL data tests against the database. If you're interested in exploring how the tests are generated and executed, the test framework lives in the `test` directory and is built using the `unittest` Python standard library package.

The test framework runs SQL queries in the `sql_tests` directory against the database. **A passing test query should not return any rows (not even a count of zero).** This means you should write each test query to select for rows that are "bad" or would not be expected in your table of interest.

To test the database with your queries in `sql_tests`, run the following command (`-v` here just adds verbose output):
```
python3 -m unittest discover -v
```

Write some SQL tests of your own in the `sql_tests` folder and run them to check your assumptions about the data. As an exercise, write tests for the following assumptions (you can reference the `solutions` branch in this repo to check your work):

- The field `camis` is never null
- The field `camis` is the primary key of the table
- All scores are positive values
- Restaurants who receive less than 13 points receive an A grade, between 14 and 27 points receive a B grade, and more than 27 points receive a C grade.
- The possible values for `borough` are Manhattan, Queens, Brooklyn, Bronx, and Staten Island

### Predicting restaurant inspection scores

Running the following command will attempt to predict the most recent score of each restaurant using a time-decayed weighted average of the past inspections.
```
python3 predict.py
```

Here's how the prediction code works:

We remove the most recent inspection (`sequence = 1`) from the dataset, since we're trying to predict it. We apply a weight to each historical inspection based on how many days prior to the 2nd-to-last inspection (`sequence = 2`).

We apply this function using an [exponential decay function](https://en.wikipedia.org/wiki/Exponential_decay) `w = power(2, -d / h)`, where `w` is the weight for that inspection, `d` is the number of days between the inspection and the 2nd-to-last inspection, and `h` is a half-life in days. I use a half-life of 365 days, since inspections are conducted roughly annually.

Finally, we calculate a [weighted average](https://en.wikipedia.org/wiki/Weighted_arithmetic_mean) of the scores for all historical inspections.

We calculate the [mean absolute error](https://en.wikipedia.org/wiki/Mean_absolute_error) of each prediction by taking the average absolute difference between the predicted inspection score and the actual inspection score.

### Testing the prediction code
In addition to testing the quality of our data, we can also test the way each transformation and prediction method works. We can have the fanciest predictive model in the world, but it will always fail if our data extraction and cleaning code is flawed.

To test our code, we'll use the `unittest` Python package. The documentation describes how to get started, but the general principle is to create a subclass of `unittest.TestCase` with a bunch of methods that start with `test_`. This subclass gets tons of useful `assert` methods from `unittest` used to test different assumptions.

Once you've added some test cases and test methods, run the following command (`-v` here just adds verbose output):
```
python3 -m unittest discover -v
```

You can run specific tests by running variants of these commands:
```
# Test the entire module
python3 -m unittest test.test_predict
# Test a specific test case
python3 -m unittest test.test_predict.PredictTest
# Test a specific test method
python3 -m unittest test.test_predict.PredictTest.test_mae_is_always_positive
```
